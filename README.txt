* Implementation Mechanism for Assignment 3
* Written by Pang Luo 
* Last Modified on 21/05/2016

1. Data Structure
A struct is defined to store information about a filename and the number of matches for all search terms:

typedef struct Freq {
    char *filename;
    unsigned int matches;
} freq;

For implementation convenience we define a global array (freqArray) of which each element is such a struct denoting a file.


2. Algorithm
We iterate all files in the specified folder and denote each eligible file as an element of freqArray. By eligible we mean the file matches all the search terms. To get the number of matches in a file for all search terms, we iterate each term and get the corresponding number of matches. Once a term can't be found in the file, we don't bother to search for other terms and ignore this file immediately.

To get the number of matches in a file for a search term, we read a block of bytes from the disk into a memory buffer, and calculate the number of occurrences of this term in the buffer via an efficient built-in library function strcasestr. Once we have handled the buffer, we move back the file pointer a few bytes, read a new block of bytes into the buffer in case a matched string lies around the boundary of two buffers. We repeat this process until encountering EOF.

In order to reduce the number of IOs on a file, for each search term except the first one, we detect whether the file content has already been loaded into the memory buffer completely. If so, we just search the buffer for the term without doing unneccesary disk reads.

Lastly, we implement a compare function which indicates whether a file is superior or inferior to another in accordance with the specified ranking standard so that we can call the built-in qsort function to sort the eligible files.

It turns out the implementation is simple but efficient enough so that we don't have to involve an index file.
