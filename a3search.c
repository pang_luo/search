/* 
 * Multiple Files Search
 *
 * A program that searches multiple files in a case insensitive manner
 *
 * Written by Pang Luo
 * Last modified on 21/05/2016
*/

#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define CHUNK_LEN 10000
char buffer[CHUNK_LEN + 1];

char* terms[5];  // 5 search terms at most
int totalTerms;

typedef struct Freq {
    char *filename;
    unsigned int matches;
} freq;
freq freqArray[2000]; // 2000 files at most


// return the number of matches in "buffer" for a search term
inline unsigned int occ(char *term, int termLen)
{
    char *p = buffer;
    unsigned int score = 0;

    while (1) {
	p = (char *) strcasestr(p, term);
	if (p == NULL)
	    break;
	++score;
        p += termLen;
    }

    return score;
}


// return the number of matches in a file for a search term
unsigned int get_score(FILE *f, int round, char *term)
{
    unsigned int score = 0;
    unsigned n;
    int termLen = strlen(term);
    int offset = 1 - termLen;
    // if "buffer" can accommodate the whole file, extraRead would be 0
    static int extraRead = 0;

    // we directly use "buffer" without reading from the disk if this is not
    //   for the first search term and the file content has been in "buffer"
    if (round && ! extraRead)
        return occ(term, termLen);

    extraRead = 0;
    fseek(f, 0, SEEK_SET);

    while (1) {
        n = fread(buffer, 1, CHUNK_LEN, f);
	buffer[n] = '\0';
        score += occ(term, termLen);

        if (feof(f))
	    return score;

        ++extraRead;
	// move the file pointer back for |offset| bytes to deal with the
	//   boundary condition
	fseek(f, offset, SEEK_CUR);
    }
}


// return the number of matches in a file for all search terms
unsigned int matches(char *filename)
{
    FILE *f = fopen(filename, "r");
    int i;
    // the number of matches for all search terms
    unsigned int score = 0;
    // the number of matches for a search term
    unsigned int tempScore;

    for (i = 0; i < totalTerms; ++i) {
        tempScore = get_score(f, i, terms[i]);
	// if the file doesn't match a search term, we don't bother to
	//   search for others and return 0 immediately
        if (! tempScore) {
	    fclose(f);
	    return 0;
	}
	score += tempScore;
    }
    fclose(f);

    return score;
}


int compare(const void *a, const void *b) 
{
    const freq *freq1 = a;
    const freq *freq2 = b;

    // frequency: descending order
    int diff = freq2->matches - freq1->matches;

    return diff ? diff : strcmp(freq1->filename, freq2->filename);
}


int main(int argc, char *argv[])
{
    // by default there are five arguments before the first search term
    int termsOffset = 5;
    if (strcmp(argv[3], "-s"))
        termsOffset = 3;
    totalTerms = argc - termsOffset;

    int i;
    for (i = 0; i < totalTerms; ++i)
	terms[i] = argv[termsOffset + i];

    DIR *dirp;
    struct dirent *entry;
    int n = 0; // the number of files in the folder
    dirp = opendir(argv[1]);
    chdir(argv[1]);
    if (dirp) {
        while ((entry = readdir(dirp)) != NULL) {
	    if (entry->d_type == DT_DIR) // ignore it if it is a directory
	        continue;
	    unsigned int score = matches(entry->d_name);
	    if (! score) // ignore the file unless it matches all search terms
	        continue;
	    freqArray[n].filename = strdup(entry->d_name);
	    freqArray[n].matches = score;
	    ++n;
	}
	closedir(dirp);
    }

    qsort(freqArray, n, sizeof(freq), compare);

    for (i = 0; i < n; ++i) {
        printf("%s\n", freqArray[i].filename);
	free(freqArray[i].filename);
    }

    return 0;
}
